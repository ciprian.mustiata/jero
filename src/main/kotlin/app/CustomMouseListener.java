package app;

import app.commands.MouseListenerCommand;
import app.common.ICustomMouseListener;
import com.google.inject.Inject;
import pubsub.PubSub;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CustomMouseListener implements ICustomMouseListener, MouseListener {

    private final PubSub _pubSub;

    @Inject
    public CustomMouseListener(PubSub pubSub)
    {
        _pubSub = pubSub;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        _pubSub.publishObj(new MouseListenerCommand("Clicked", e));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        _pubSub.publishObj(new MouseListenerCommand("Pressed", e));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        _pubSub.publishObj(new MouseListenerCommand("Released", e));
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        _pubSub.publishObj(new MouseListenerCommand("Entered", e));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        _pubSub.publishObj(new MouseListenerCommand("Exited", e));
    }
}
