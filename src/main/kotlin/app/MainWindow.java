package app;

import app.commands.TimerTickCommand;
import app.common.IGameMap;
import app.common.IMainWindow;
import app.gfx.MainPaintArea;
import com.google.inject.Inject;
import pubsub.PubSub;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame implements IMainWindow {
    private Timer _timer;
    private PubSub _pubSub;
    private IGameMap _gameMap;

    @Inject
    public MainWindow(
            IGameMap gameMap,
            MainPaintArea paintArea,
            PubSub pubSub,
            CustomMouseListener customMouseListener
    ) {
        super();
        _gameMap = gameMap;
        _pubSub = pubSub;

        setLayout(new BorderLayout());

        addMouseListener(customMouseListener);
        this.add(paintArea);
        paintArea.setVisible(true);

        _timer = new Timer(5, it->{ onTimerTick(System.currentTimeMillis()); });
        _timer.start();
    }

    private void onTimerTick(long currentTimeMillis) {
        _pubSub.publishObj(new TimerTickCommand(currentTimeMillis));

        repaint();
    }
}
