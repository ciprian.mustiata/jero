package app;

import com.google.inject.Guice;
import modules.ApplicationModule;

import javax.swing.*;
import java.awt.*;

public class Program {
    public static void main(String[] args) {
        var injector= Guice.createInjector(new ApplicationModule());

        var frame = injector.getInstance(MainWindow.class);
        frame.setSize(new Dimension(1920, 1080));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
