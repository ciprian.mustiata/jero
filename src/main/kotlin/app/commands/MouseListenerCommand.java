package app.commands;

import java.awt.event.MouseEvent;

public record MouseListenerCommand(String name, MouseEvent ev) {
}
