package app.commands;

public record TimerTickCommand(Long currentTimeMillis) {
}
