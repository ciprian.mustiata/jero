package app.common;

public interface IGameMap {
    int getWidth();
    int getHeight();
    void setSize(int width, int height);
}
