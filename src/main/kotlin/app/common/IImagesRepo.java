package app.common;

import app.gfx.ImagePicture;
import java.util.List;

public interface IImagesRepo {
    List<ImagePicture> scanDir(String dirName) ;
}
