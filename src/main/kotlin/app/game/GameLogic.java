package app.game;

import app.commands.TimerTickCommand;
import app.common.IGameLogic;
import com.google.inject.Inject;
import pubsub.PubSub;

public class GameLogic implements IGameLogic {
    private PubSub _pubSub;

    @Inject
    public GameLogic(PubSub pubSub) {
        _pubSub = pubSub;

        _pubSub.subscribe(this::onTimerEvent, TimerTickCommand.class);
    }

    private void onTimerEvent(TimerTickCommand it) {

    }
}
