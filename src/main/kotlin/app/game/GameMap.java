package app.game;

import app.common.IGameMap;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class GameMap implements IGameMap {
    private int _width = 64;
    private int _height = 64;
    private List<TileMap> _map = new ArrayList<>();

    public GameMap() {
        setSize(_width, _height);
    }

    public void setSize(int width, int height) {
        _map.clear();
        _width = width;
        _height = height;
        IntStream.range(0, width * height)
                .forEach(it -> {
                    _map.add(new TileMap((byte) 0, (byte) 0));
                });
    }

    public int getWidth() {
        return _width;
    }

    public int getHeight() {
        return _height;
    }

    public String toString() {
        return _width + "," + _height;
    }

}
