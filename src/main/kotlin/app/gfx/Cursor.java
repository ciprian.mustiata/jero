package app.gfx;

import app.common.ICursor;
import app.common.IImagesRepo;
import com.google.inject.Inject;
import pubsub.PubSub;

import java.awt.*;


public class Cursor implements ICursor {
    private final IImagesRepo _images;
    CursorMousePosition _position = new CursorMousePosition(0, 0);

    @Inject
    public Cursor(IImagesRepo images, PubSub pubSub){
        this._images = images;
        pubSub.subscribe(this::onEvent, CursorMousePosition.class);
    }
    
    void onEvent(CursorMousePosition position) {
        _position = position;
    }
    
    void paint(Graphics2D graphics2D){
    }
}
