package app.gfx;

public record CursorMousePosition(int x, int y){}