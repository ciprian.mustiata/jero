package app.gfx;

import java.awt.image.BufferedImage;

public record ImagePicture (String fileName, BufferedImage img){
}
