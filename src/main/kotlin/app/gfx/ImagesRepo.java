package app.gfx;

import app.common.IImagesRepo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ImagesRepo implements IImagesRepo {

    private static Stream<Path> getFilesInDir(String dirName) {
        try {
            return
                    Files.walk(Paths.get(dirName))
                            .filter(Files::isRegularFile);
        } catch (IOException e) {
        }

        return Arrays.stream(new Path[0]);
    }

    @Override
    public List<ImagePicture> scanDir(String dirName) {
        ArrayList<ImagePicture> result = new ArrayList<>();
        getFilesInDir(dirName)
                .parallel()
                .map(it -> {
                    BufferedImage imageItem = null;
                    try {
                        imageItem = ImageIO.read(it.toFile());

                        return new ImagePicture(it.toFile().getCanonicalPath(), imageItem);
                    } catch (IOException e) {
                        return null;
                    }
                })
                .sequential()
                .forEach(result::add);
        return result;
    }

}
