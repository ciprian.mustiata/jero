package app.gfx;

import app.common.IGameMap;
import app.common.IImagesRepo;
import app.common.IPaintArea;
import com.google.inject.Inject;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.stream.IntStream;

public class MainPaintArea extends JPanel implements IPaintArea {

    private List<ImagePicture> _images;

    @Inject
    public MainPaintArea(IGameMap gameMap, IImagesRepo images) {
        setSize(1920, 1080);
        setVisible(true);
        _images = images.scanDir("assets/gfx");
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.YELLOW);
        g.fillRect(0, 0, 40 * 64, 30 * 64);

        IntStream.range(0, 4).forEach(i -> {
            IntStream.range(0, 3).forEach(j -> {
                g.drawImage(_images.get(2).img(), i * 1024, j * 1024, null);
            });
        });
        g.dispose();
    }
}
