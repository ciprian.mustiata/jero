module jero {
    requires java.desktop;
    requires com.google.guice;

    exports pubsub;
    exports app;
    exports app.game;
    exports app.common;
    exports app.gfx;
}