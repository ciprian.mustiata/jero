package modules;

import app.CustomMouseListener;
import app.MainWindow;
import app.common.*;
import app.game.GameLogic;
import app.game.GameMap;
import app.common.IImagesRepo;
import app.gfx.*;
import pubsub.IPubSub;
import pubsub.PubSub;

public class ApplicationModule extends SkeletonModuleUtilities {
    @Override
    protected void configure() {

        addSingleton(IPubSub.class, PubSub.class);
        addSingleton(IGameMap.class, GameMap.class);
        addSingleton(IGameLogic.class, GameLogic.class );
        addSingleton(ICustomMouseListener.class, CustomMouseListener.class);
        addSingleton(IImagesRepo.class, ImagesRepo.class );
        addSingleton(IPaintArea.class, MainPaintArea.class);
        addSingleton(ICursor.class, Cursor.class);
        addSingleton(IMainWindow.class, MainWindow.class);

    }
}
