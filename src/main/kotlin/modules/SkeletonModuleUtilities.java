package modules;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;

public abstract class SkeletonModuleUtilities extends AbstractModule {

    protected <TInterface> SkeletonModuleUtilities addSingleton(Class<TInterface> interfaceClazz, Class<? extends TInterface> clazz) {
        bind(interfaceClazz).to(clazz).in(Scopes.SINGLETON);
        return this;
    }
}
