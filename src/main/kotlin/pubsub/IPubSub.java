package pubsub;

import java.util.function.Consumer;

public interface IPubSub {
    <T> void subscribeT(Consumer<T> onEvent, Class<T> clazz);
    <T> void publishT(T eventData, Class<?> clazz);
}
