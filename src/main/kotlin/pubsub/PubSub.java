package pubsub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

public class PubSub implements IPubSub {
    private final Map<String, ArrayList<Consumer<Object>>> _clients = new HashMap<>();

    @Override
    public <T> void subscribeT(Consumer<T> onEvent, Class<T> clazz) {

        var className = clazz.getCanonicalName();
        var listOfClients = _clients.getOrDefault(className, null);
        if (Objects.isNull(listOfClients)) {
            listOfClients = new ArrayList<>();
            _clients.put(className, listOfClients);
        }
        listOfClients.add(it -> onEvent.accept((T) it));
    }

    @Override
    public <T> void publishT(T eventData, Class<?> clazz) {
        var className = clazz.getCanonicalName();
        var listOfClients = _clients.getOrDefault(className, null);
        if (Objects.isNull(listOfClients)) {
            return;
        }
        for (var client : listOfClients) {
            client.accept(eventData);
        }

    }

    public void publishObj(Object eventData){
        publishT(eventData, eventData.getClass());
    }

    public <T> void subscribe(Consumer<T> onEvent, Class<T> clazz) {
        var className = clazz.getCanonicalName();
        var listOfClients = _clients.getOrDefault(className, null);
        if (Objects.isNull(listOfClients)) {
            listOfClients = new ArrayList<>();
            _clients.put(className, listOfClients);
        }
        listOfClients.add(it -> onEvent.accept((T) it));
    }
}
